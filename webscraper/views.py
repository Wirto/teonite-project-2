from django.db import models
from webscraper.models import *
from rest_framework import viewsets
from webscraper.serializers import *
from django.db.models import Sum



class StatsViewSet(viewsets.ModelViewSet):
    queryset = Wordinarticle.objects.values('word__value').annotate(count=Sum('count')).order_by('-count')
    serializer_class = WordinarticleSerializer

class AuthorsViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.values('name')
    serializer_class = AuthorSerializer



