from webscraper.models import *
from rest_framework import serializers

class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Author
        fields = ('name',)

class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.StringRelatedField()
    class Meta:
        model = Article
        fields = ('title', 'author')

class WordSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Word
        fields = ('value',)

class WordinarticleSerializer(serializers.HyperlinkedModelSerializer):
    word = serializers.ReadOnlyField(source='word__value')
    class Meta:
        model = Wordinarticle
        fields = ('count','word')

