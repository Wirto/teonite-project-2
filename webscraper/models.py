from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
class Article(models.Model):
    title = models.CharField(max_length=255)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
        
    def __str__(self):
        return self.title
class Word(models.Model):
    value = models.CharField(max_length=50)
    
    def __str__(self):
        return self.value
class Wordinarticle(models.Model):
    count = models.IntegerField()
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    word = models.ForeignKey(Word, on_delete=models.CASCADE)


