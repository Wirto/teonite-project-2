from django.urls import include, path
from rest_framework import routers
from webscraper import views

router = routers.DefaultRouter()
router.register(r'stats', views.StatsViewSet)
router.register(r'authors', views.AuthorsViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]