import scrapy

class ArticleItem(scrapy.Item):
    author = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()
