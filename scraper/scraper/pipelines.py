from webscraper.models import *
from django.db.models import F

class ScraperPipeline(object):
    def process_item(self, item, spider):
        while(True):
            try:
                author = Author.objects.get(name=item["author"])
                break
            except Author.DoesNotExist:
                author = Author()
                author.name = item["author"]
                author.save()
                break
        while(True):
            try:
                article = Article.objects.get(title=item["title"])
                break
            except Article.DoesNotExist:
                article = Article()
                article.title = item["title"]
                article.author= Author.objects.get(name=item["author"])
                article.save()
                break 

        while(True):
            try:
                word = Word.objects.get(value=item["content"])
                break
            except Word.DoesNotExist:
                word = Word()
                word.value = item["content"]
                word.save()
                break

        while(True):
            try:

                word = Word.objects.get(value=item["content"])
                word.save()
                article = Article.objects.get(title=item["title"])
                article.save()
                wordinarticle = Wordinarticle.objects.get(word=word, article=article)
                wordinarticle.count += 1
                wordinarticle.save()

                
                break
            except Wordinarticle.DoesNotExist:
                wordinarticle = Wordinarticle()
                wordinarticle.word = Word.objects.get(value=item["content"])
                wordinarticle.article = Article.objects.get(title=item["title"])
                wordinarticle.count = 1
                wordinarticle.save()
                break
        return item