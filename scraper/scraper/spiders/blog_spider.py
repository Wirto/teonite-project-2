import scrapy
from scrapy import signals
from ..items import ArticleItem
from webscraper.models import *

class SpiderBlog(scrapy.Spider):
    name = "blog"
    start_urls = ['https://teonite.com/blog/']

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(SpiderBlog, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_opened, signal=signals.spider_opened)
        return spider

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s', spider.name)
        Author.objects.all().delete()
        Word.objects.all().delete()
        Wordinarticle.objects.all().delete()
        Article.objects.all().delete()

    def parse(self,response):
        nextpage = response.css("li.blog-button a::attr(href)")[-1].get()

        links = response.css("h2.post-title a::attr(href)").getall()
        for link in links:
            yield response.follow(link, self.parse2)
        if nextpage is not None:
            yield response.follow(nextpage, self.parse)

    def parse2(self,response):
        stopwords = ["and","the","\n", ".", ",", "-", "\\", "/", "(",")","?","!","’"]
        content = "".join(response.css("div.post-content *::text").getall()).lower()

        for stopword in stopwords:
            content = content.replace(stopword, " ")
        
        content = content.split()
        author = response.css("div.author-text span.author-name strong::text").get()
        title = response.css("h1.post-title::text").get()

        for word in content:
            if len(word)>2:
                articleItem = ArticleItem(title=title,author=author, content= word)
                yield articleItem 

